#!/usr/bin/python
#-*- coding: utf-8 -*-

# Author: Adrian Czapla
# Date:   2016

################################################################################

from math import sqrt

################################################################################

class Vector:
    def __init__( _, x=0.0, y=0.0, z=0.0 ):
        _.x = x
        _.y = y
        _.z = z

    def __str__( _ ): return ''.join( ['(', str(_.x), ', ', str(_.y), ', ', str(_.z), ')\r\n'] )

    # "Overloading" mathematical operators:
    def __neg__( _ ): # -_ | *= -1
        return Vector( - _.x, - _.y, - _.z )
    def __add__( _, v ): # _ + v
        if (isinstance(v, Vector)) : return Vector( _.x + v.x, _.y + v.y, _.z + v.z )
        else                       : return Vector( _.x + v,   _.y + v,   _.z + v   )
    def __radd__( _, v ): # reverse add | v + _
        return _ + v
    def __iadd__( _, v ): # +=
        return _ + v
    def __sub__( _, v ):
        if (isinstance(v, Vector)) : return Vector( _.x - v.x, _.y - v.y, _.z - v.z )
        else                       : return Vector( _.x - v,   _.y - v,   _.z - v   )
    def __rsub__( _, v ): # reverse sub | v - _
        if (isinstance(v, Vector)) : return Vector( v.x - _.x, v.y - _.y, v.z - _.z )
        else                       : return Vector(   v - _.x,   v - _.y,   v - _.z )
    def __isub__( _, v ): # -=
        return _ - v
    def __mul__( _, v ):
        if (isinstance(v, Vector)) : return Vector( _.x * v.x, _.y * v.y, _.z * v.z )
        else                       : return Vector( _.x * v,   _.y * v,   _.z * v   )
    def __rmul__( _, v ): # reverse mul | v * _
        return _ * v
    def __imul__( _, v ): # *=
        return _ * v
    def __div__( _, v ):
        if (isinstance(v, Vector)) : return Vector( _.x / v.x, _.y / v.y, _.z / v.z )
        else                       : return Vector( _.x / v,   _.y / v,   _.z / v   )
    def __truediv__( _, v ): # as opposed to __floordiv__
        return _.__div__( v )
    def __rdiv__( _, v ): # reverse div | v / _
        if (isinstance(v, Vector)) : return Vector( v.x / _.x, v.y / _.y, v.z / _.z )
        else                       : return Vector(   v / _.x,   v / _.y,   v / _.z )
    def __rtruediv__( _, v ):
        return _.__rdiv__( v )
    def __idiv__( _, v ): # /=
        return _ / v
    def __itruediv__( _, v ):
        return _.__idiv__( v )

    def dotProduct( _, v ):
        return _.x*v.x + _.y*v.y + _.z*v.z
    def length( _ ):
        return sqrt( _.dotProduct(_) )
    def distance( _, v ):
        return (_ - v).length()

################################################################################
