#!/usr/bin/python
#-*- coding: utf-8 -*-

# Author: Adrian Czapla
# Date:   2016

################################################################################

import pygame
import random
#from OpenGL.GL import *
#from OpenGL.GLU import *

from globals import *
from vector import Vector as V
from util import sgn, display_message
from painter import paint_all_things
from sound import *

#print dir(pygame.time)
#print help(pygame.time.get_ticks)

################################################################################

class Creature:
    def __init__( self, r, m ):
        self.m        = m
        self.r        = r
        self.v        = V()
        self.r_former = r
        self.v_former = V()
        self.F        = V()
        self.img    = actor_img
        self.width  = self.img.get_width()
        self.height = self.img.get_height()

    def __str__( self ):
        return ''.join( ['r = ', str(self.r), 'v = ', str(self.v)] )

    def display_stats( self ):
        display_message( str(points_gathered), 0.09, 0.072, 36, color=(255, 255, 0) )
        if show_stats :
            color = (255, 255, 255)
            display_message( ''.join( 'X Force: '    + str(round(self.F.x, 2)) ), 0.9, 0.05, 15, color=color )
            display_message( ''.join( 'Y Force: '    + str(round(self.F.y, 2)) ), 0.9, 0.10, 15, color=color )
            display_message( ''.join( 'X Velocity: ' + str(round(self.v.x, 2)) ), 0.9, 0.15, 15, color=color )
            display_message( ''.join( 'Y Velocity: ' + str(round(self.v.y, 2)) ), 0.9, 0.20, 15, color=color )
            display_message( ''.join( 'X Position: ' + str(round(self.r.x, 2)) ), 0.9, 0.25, 15, color=color )
            display_message( ''.join( 'Y Position: ' + str(round(self.r.y, 2)) ), 0.9, 0.30, 15, color=color )

    def euler( self ): # Reverse Euler integration method
        dv      = self.F / self.m * dt
        self.v += dv
        dr      = self.v * dt * 30
        self.r += dr

    def move( self, F ):
        self.r_former = self.r
        self.v_former = self.v
        self.evaluate_forces( F )
        self.euler()
        self.apply_limits()
        self.collide()

    def evaluate_forces( self, F ): # F V already holds some values at this point, given by direction keys
        if not underwater_level :
            F.y += self.m * g                                          # gravity
            F.x += - sgn(self.v.x) * air_resistance * self.m * g       # drag (air resistance)
            F.y += - sgn(self.v.y) * air_resistance * self.m * g       # drag (air resistance)
            if self.is_on_the_ground() :
                F.x += - sgn(self.v.x) * surface_friction * self.m * g # surface friction
        else :
            F.y += self.m * (g - buoyancy)                             # gravity & buoyancy
            F.x += - sgn(self.v.x) * water_resistance * self.m * g     # drag (fluid friction)
            F.y += - sgn(self.v.y) * water_resistance * self.m * g     # drag (fluid friction)
            if self.is_on_the_ground() :
                F.x += - sgn(self.v.x) * surface_friction * self.m * g # surface friction
        self.F = F

    def apply_limits( self ):
        # speed limits
        if abs(self.v.x) > v_max : self.v.x = sgn(self.v.x) * v_max
        if abs(self.v.y) > v_max : self.v.y = sgn(self.v.y) * v_max
        v_min = 0.15
        if abs(self.v.x) < v_min :
            self.v.x        = 0
            self.v_former.x = 0
        if abs(self.v.y) < v_min :
            self.v.y        = 0
            self.v_former.y = 0

        # walls
        # if (self.r.x <= self.width/2             and self.v.x < 0) or \
        #    (self.r.x >= display_w - self.width/2 and self.v.x > 0) or \
        #    (self.r.y <= self.height/2            and self.v.y < 0) :
        #         self.r -= self.v * dt * 30
        #         self.v *= - elasticity

        # self.v = self.v - (
        #     self.r - p2.r * (
        #         (self.v - p2.v).dotProduct(self.r - p2.r)
        #         / pow(self.r.distance(p2.r), 2)
        #         * 2 * 1.0 / (1.0 + 1.0)#p2.mass / (p1.mass + p2.mass)
        #     )
        # )

    def collide( self ):
        for obstacle in objects['obstacles'] :
            if obstacle.collide( self ) :
                break ##############

    def is_on_the_ground( self ):
        global won
        ret = False
        for obstacle in objects['obstacles'] :
            if self.r.x + self.width/2 >= obstacle.walls()['l'] and \
               self.r.x - self.width/2 <= obstacle.walls()['r'] :
                if abs(obstacle.walls()['t'] - (self.r.y + self.height/2)) < 1 :
                    if obstacle.winning :
                        won = True
                        game_over()
                    ret = True
                    #display_message( 'is on the ground', x=0.9, y=0.95, update=True )
                    break ######################################################
        return ret

    def draw( self ):
        display.blit( self.img, (display_w/2 - self.width/2,
                                    self.r.y - self.height/2) )

################################################################################

class Obstacle( object ): # new-style class
    def __init__( self, r, v, m, width, height,
                  winning=False, phantom=False, color=(128, 128, 128) ):
        self.m       = m
        self.r       = r
        self.v       = v
        self.width   = width
        self.height  = height
        self.color   = color
        self.phantom = phantom
        self.winning = winning
        self.image = pygame.image.load( 'brick.jpg' )
        self.image = pygame.transform.scale(  self.image,
                                             (self.width, self.height) )

    def walls( self ):
        return {
            't' : self.r.y - self.height/2,
            'b' : self.r.y + self.height/2,
            'l' : self.r.x - self.width/2,
            'r' : self.r.x + self.width/2
        }

    def draw( self ):
        display.blit( self.image,
            ( self.walls()['l'] - objects['actor'].r.x + display_w/2,
              self.walls()['t'] )
        )
        #pygame.draw.circle( display, color, [pos.x, pos.y], obstacle_w )

    def collide( self, creature ):
        global continue_game
        ret = False
        crit = creature
        if  crit.r.y + crit.height/2 >= self.walls()['t'] and \
            crit.r.y - crit.height/2 <= self.walls()['b'] and \
            crit.r.x + crit.width/2  >= self.walls()['l'] and \
            crit.r.x - crit.width/2  <= self.walls()['r'] :
                # distances between us and the walls of the obstacle
                t_dist = abs(crit.r.y + crit.height/2 - self.walls()['t']) # top
                b_dist = abs(crit.r.y - crit.height/2 - self.walls()['b']) # bottom
                l_dist = abs(crit.r.x + crit.width/2  - self.walls()['l']) # left
                r_dist = abs(crit.r.x - crit.width/2  - self.walls()['r']) # right
                if min( [t_dist, b_dist, l_dist, r_dist] ) == t_dist \
                 and t_dist < 20 \
                 and not isinstance(self, Coin) :
                # no collision on x axis - we're on the ground, return False.
                    crit.r.y = self.walls()['t'] - crit.height/2 # put ourselves back precisely on the ground level
                    crit.v.y *= - elasticity
                elif self.v.length() != 0 \
                 and min( [t_dist, b_dist, l_dist, r_dist] ) == b_dist \
                 and b_dist < 6 :
                # if this obstacles is a moving obstacle and we're under it
                    game_over() ##############################
                else :
                # collide on all axes, return True.
                    ret = True
                    jump_land_sound.play()
                    if not self.phantom :
                        crit.r -= (crit.v - self.v) * dt * 30
                        crit.v *= - elasticity
        return ret

    def move( self ):
        if self.r.y - self.height/2 > display_h:
           self.r = V( random.randrange( self.width, display_w - self.width ),
                       0, 0 )
        self.r += self.v

################################################################################

class Coin( Obstacle ):
    coins_count = 0
    @classmethod
    def recount_coins( cls, val ):
        cls.coins_count += val

    def __init__( self, r ):
        Coin.recount_coins( 1 )
        self.r = r
        self.v = V()
        self.F = V()
        self.m = 1.0
        self.phantom = True
        self.winning = False
        self.width  = coin_sprite_h
        self.height = coin_sprite_h

    @classmethod
    def draw_at( cls, x, y ):
        time = pygame.time.get_ticks()
        coin_img = pygame.Surface( (coin_sprite_h, coin_sprite_h),
                                    pygame.SRCALPHA, 32 )
        coin_img.blit(
            coin_sprite, (0, 0), (
                time       * coin_sprite_h % coin_sprite_w, 0,
                (time + 1) * coin_sprite_h % coin_sprite_w, coin_sprite_h ))
        display.blit( coin_img, [x - objects['actor'].r.x + display_w/2, y] )

    def draw( self ):
        Coin.draw_at( self.walls()['l'], self.walls()['t'] )
        # pygame.draw.rect( display, (255, 0, 0),
        #             [self.walls()['l'] - objects['actor'].r.x + display_w/2,
        #              self.walls()['t'],
        #              self.width, self.height] )

    def collide( self, creature ):
        global points_gathered
        if super(Coin, self).collide( creature ) :
            #del objects['obstacles'][objects['obstacles'].index(self)]
            #objects['obstacles'].remove(objects['obstacles'].index(self))
            objects['obstacles'].pop(objects['obstacles'].index(self))
            Coin.recount_coins( -1 )
            points_gathered += 1
            print( 'Coin stolen :<, ' + str(Coin.coins_count) + ' left...' )
            coin_sound().play()
        return False

################################################################################

def play_game():
    global underwater_level, continue_game, objects, show_stats

    p = Creature( V( 50, display_h*3/5, 0 ), 1.0 ) # actor center x, y, z, actor mass
    objects['actor'] = p

    objects['obstacles'] = [
        # ledges:
        Obstacle( V(80,             500, 0), V(), 1.0, 180, 30 ),
        Obstacle( V(display_w - 80, 500, 0), V(), 1.0, 180, 30 ),
        Obstacle( V(display_w/2,    400, 0), V(), 1.0, 100, 30 ),
        Obstacle( V(550,            300, 0), V(), 1.0, 100, 30 ),
        Obstacle( V(100,            200, 0), V(), 1.0, 100, 30 ),
        # moving obstacles:
        Obstacle(
            V( random.randrange( 100, display_w - 100 ), 0, 0 ), # position
            V(0, 3, 0), # velocity
            1.0, # mass
            100, 100 # size
        ),
        # coins:
        Coin( V(80,             450, 0) ),
        Coin( V(display_w - 80, 450, 0) ),
        Coin( V(display_w/2,    350, 0) ),
        Coin( V(550,            250, 0) ),
        Coin( V(100,            150, 0) )
    ]

    while continue_game :
        F = V()

        # key-downs and key-ups
        for event in pygame.event.get() :
            if event.type == pygame.QUIT :
                continue_game = False ########################
            elif event.type == pygame.MOUSEBUTTONUP and event.button == 1 :
                show_stats ^= True
            if event.type == pygame.KEYDOWN :
                if event.key in (pygame.K_q, pygame.K_ESCAPE) :
                    continue_game = False ###############
                elif not underwater_level :
                    if event.key == pygame.K_UP and p.is_on_the_ground() :
                        F.y -= jump_F # jump if on the ground
                        jump_sound.play()

        # keys being pressed currently
        keys = pygame.key.get_pressed()
        if   keys[pygame.K_LEFT]  : F.x -= key_F
        elif keys[pygame.K_RIGHT] : F.x += key_F
        if underwater_level :
            if   keys[pygame.K_UP]   : F.y -= key_F
            elif keys[pygame.K_DOWN] : F.y += key_F
        else :
            if   keys[pygame.K_DOWN] : F.y += key_F / 3

        p.move( F )
        for obstacle in objects['obstacles'] :
            obstacle.move()

        if p.r.y >= display_h - p.height/2 :
            game_over()

        paint_all_things( underwater_level )
        Coin.draw_at( objects['actor'].r.x - display_w/2, 12 )
        pygame.display.update()
        clock.tick( frames_per_second )

        is_game_over()

    display_message( 'Game Over',
                      font_size=115, color=(205, 0, 0), update=True )
    if won :
        display_message( 'You Won :D!', y=0.35,
                          font_size=115, color=(100, 255, 40), update=True )

def start_game():
    global next_game, continue_game, won, underwater_level, \
           points_gathered, Coin
    continue_game = True
    won = False
    points_gathered = 0
    Coin.coins_count = 0

    display_message( 'Press E, W (underwater level) or ESC.',
                      y=0.6, font_size=40, color=(255, 255, 255), update=True )

    K_w, K_e, K_q, K_ESCAPE = pygame.K_w, pygame.K_e, \
                              pygame.K_q, pygame.K_ESCAPE
    while True :
        for event in pygame.event.get() :
            if event.type == pygame.KEYDOWN :
                if event.key in (K_w, K_e, K_q, K_ESCAPE) :
                    if   event.key == K_w      : underwater_level = True
                    elif event.key == K_e      : underwater_level = False
                    elif event.key == K_ESCAPE :
                        game_over()
                        next_game = False ###############
                    break ######################################################
            if event.type == pygame.QUIT :
                game_over()
                next_game = False #######################
                break ##########################################################
        else: continue # executed if 'for' loop ended without 'break'
        break          # executed if 'continue' was skipped because of 'break'

def is_game_over():
    global won
    if Coin.coins_count == 0 :
        won = True
        game_over()

def game_over():
    global continue_game
    continue_game = False ###############################

################################################################################

pygame.init()
#pygame.mixer.music.load('')
#pygame.mixer.music.play()

next_game = True
while next_game :
    start_game()
    play_game()
    #answer = raw_input('Do you want play again?')
    #if answer == 'n':
    #    next_game = False

pygame.mixer.quit() # initialized at "sound.py" file
pygame.quit()
quit()
