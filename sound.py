#!/usr/bin/python
#-*- coding: utf-8 -*-

# Author: Adrian Czapla
# Date:   2016

################################################################################

import pygame
import random

from util import count_files_at

################################################################################

pygame.mixer.init()

jump_land_sound = pygame.mixer.Sound('sound/jump_land.wav')
jump_sound = pygame.mixer.Sound('sound/jump.wav')

coin_sounds = [None] * 10
for i in range(0, len(coin_sounds)):
    coin_sounds[i] = pygame.mixer.Sound('sound/Coin' + str(i+1) + '.wav')
def coin_sound():
    coin_sound = coin_sounds[random.randrange(0, len(coin_sounds))]
    return coin_sound

################################################################################
