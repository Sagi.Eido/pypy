#!/usr/bin/python
#-*- coding: utf-8 -*-

# Author: Adrian Czapla
# Date:   2016

################################################################################

import pygame
import os, os.path

from globals import display, display_w, display_h

################################################################################

def sgn( x ):
    return x / abs(x) if x != 0 else 0

################################################################################

def display_message( text, x=0.5, y=0.5, font_size=15, color=(0, 0, 0),
                     font_style='freesansbold.ttf', update=False ):
    x *= display_w
    y *= display_h
    font = pygame.font.Font( font_style, font_size )
    text_surf = font.render( text, True, color )
    text_rect = text_surf.get_rect()
    text_rect.center = (x, y)
    display.blit( text_surf, text_rect )
    if update : pygame.display.update()

################################################################################

def count_files_at( DIR ):
    return len([name for name in os.listdir(DIR)
                    if os.path.isfile(os.path.join(DIR, name))])

################################################################################
