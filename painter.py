#!/usr/bin/python
#-*- coding: utf-8 -*-

# Author: Adrian Czapla
# Date:   2016

################################################################################

import random
from math import sin, pi

from globals import *

################################################################################

ww = world_width = 4000
cp = cloud_positions  = [None] * 30 # initialize array of size...
bp = bubble_positions = [None] * 50
for i in range(0, len(cp)) :
    cp[i] = [ - ww/2 + ww * i/len(cp),
              random.randrange(0, display_h) ]
for i in range(0, len(bp)) :
    bp[i] = [ random.randrange(- ww/2, ww/2),
              random.randrange(- 50, display_h + 50) ]
def paint_all_things( underwater_level ):
    global objects, cp, bp
    p = objects['actor']
    time = pygame.time.get_ticks()

    # background
    background = pygame.Surface(display.get_size())
    if not underwater_level : background.fill((0, 200, 255))
    else                    : background.fill((100, 100, 255))
    display.blit(background, (0, 0))

    # clouds and bubbles
    if not underwater_level :
        for i in range(0, len(cp)) :
            if cp[i][0] > ww/2 + 50 or cp[i][0] < - 50 - ww/2 :
                cp[i] = [- 50 - ww/2, random.randrange(0, display_h * 2/3)]
            else :
                cp[i][0] += 4
            display.blit( cloud_img, [ cp[i][0] - objects['actor'].r.x + display_w/2,
                  cp[i][1] ] )
    else :
        for i in range(0, len(bp)) :
            if bp[i][1] > display_h + 50 or bp[i][1] < - 100 :
                bp[i] = [random.randrange(- ww/2, ww/2), display_h + 50]
            else :
                bp[i][1] -= 3
                bp[i][0] += sin(time * pi/180) + random.randrange(-2, 2)
            display.blit( bubble_img,
                [ bp[i][0] - objects['actor'].r.x + display_w/2,
                  bp[i][1]] )
    
    # else
    p.draw()
    for obstacle in objects['obstacles'] :
        obstacle.draw()
    p.display_stats()
    pygame.display.update()

################################################################################
