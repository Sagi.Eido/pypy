#!/usr/bin/python
#-*- coding: utf-8 -*-

# Author: Adrian Czapla
# Date:   2016

################################################################################

import pygame
import pygame.image as img
from pygame import transform

################################################################################

display_w, display_h = 800, 600
display = pygame.display.set_mode( (display_w, display_h) )
pygame.display.set_caption( 'Springifilous' )

#world_img = img.load( 'background.png' )
actor_img = img.load( 'actor.png' )
actor_img = transform.scale( actor_img, (actor_img.get_width() // 5, actor_img.get_height() // 5) )
cloud_img = img.load( 'lakitu_cloud.png' )
cloud_img = transform.scale( cloud_img, (cloud_img.get_width() // 4, cloud_img.get_height() // 4) )
bubble_img = img.load( 'bubble.png' )
bubble_img = transform.scale( bubble_img, (bubble_img.get_width() // 8, bubble_img.get_height() // 8) )
coin_sprite = img.load('coin_gold.png')
coin_sprite = transform.scale( coin_sprite, (coin_sprite.get_width() * 2, coin_sprite.get_height() * 2) )
coin_sprite_w, coin_sprite_h = coin_sprite.get_width(), coin_sprite.get_height()
coin_img = pygame.Surface( (coin_sprite_h, coin_sprite_h), pygame.SRCALPHA, 32 )

clock = pygame.time.Clock()
frames_per_second = 24

dt = 1. / frames_per_second
g, buoyancy = 9.81, 15.0
key_F, jump_F, v_max = 30., 210., 10.
air_resistance, water_resistance, surface_friction, elasticity = 0.15, 0.4, 0.5, 0.3

underwater_level = False
continue_game = True
won = False
points_gathered = 0
objects = {}

show_stats = False

################################################################################
